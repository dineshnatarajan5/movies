import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.themoviedb.org/3' ,
    headers: {
        Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5MGEwMWYzOTBhMzg5ZWE4MGRhM2RlMDQ4YzBlOWVkMiIsInN1YiI6IjVmNjgxNzQ4ZTE4ZTNmMDAzYTBmMWI1ZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.eVDRkqltNmNNIygGgj1KLe-OlO7F7dGtnvquylftcd0'
    },
});