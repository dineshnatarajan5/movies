import { AUTH_BASE, API_Key, Authorisation } from '../../../configs';
import Movies from '../MoviesApi'

// export default async() => {
//     var result;
//     const Token = await Movies.get('/authentication/token/new', {
//         params: {
//         api_key: "90a01f390a389ea80da3de048c0e9ed2"
//         }
//     })
//     .then((res) =>{
//         console.log(res.data,"123456");
//         result = res.data;
//     })
//     .catch((e)=>{
//         console.log(e)
//     })
//     console.log(result,"result")
//     return result;
// }
export const Token = async () => {
    return fetch(`${AUTH_BASE}/authentication/token/new`, {
        headers: {
            'Authorization': `Bearer ${Authorisation}`
        },
        params:{
            api_key: `${API_Key}`
        },
        method: 'GET'
    })
    .then((res) => {
        console.log(res, 'inToken');
        return res.json();
    })
    .catch((e) => {
        console.log(e);
    })
}

export const session = async () => {
    return fetch(`${AUTH_BASE}/authentication/session/new`, {
        headers: {
            'Authorization': `Bearer ${Authorisation}`
        },
        params:{
            api_key: `${API_Key}`
        },
        method: 'POST',
        body:JSON.stringify({
            request_token
        }),
    })
    .then((res) => {
        console.log(res, 'inSession');
        return res.json();
    })
    .catch((e) => {
        console.log(e);
    })
}

export const LoginUser = async () => {
    return fetch(`${AUTH_BASE}/authentication/session/new`, {
        headers: {
            'Authorization': `Bearer ${Authorisation}`
        },
        params:{
            api_key: `${API_Key}`
        },
        method: 'POST',
        body:JSON.stringify({
            "username": "johnny_appleseed",
            "password": "test123",
            "request_token": request_token,
        }),
    })
    .then((res) => {
        console.log(res, 'inLogin');
        return res.json();
    })
    .catch((e) => {
        console.log(e);
    })
}