import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import Movies from '../Screens/Movies/Movies'
import TVShows from '../Screens/TVShows/TVShows'
import Kids from '../Screens/Kids/Kids'
import Podcast from '../Screens/Podcast/Podcast'
import Home_inner from '../Screens/Home_in_Home/Home_inner';

const Tab_nav = createAppContainer(createMaterialTopTabNavigator(
    {
        Home:{
            screen: Home_inner,
        },
        TVShows: {
            screen: TVShows,
        },
        Movies: {
            screen: Movies,
        },
        Kids: {
            screen: Kids,
        },
    },{
        tabBarOptions:{
            style:{
                backgroundColor: '#0f171e'
            },
            indicatorStyle:{
                backgroundColor:'white'
            },
            
        }
    }
));

export default Tab_nav;