import {createBottomTabNavigator} from 'react-navigation-tabs';

import Home from '../Screens/Home/Home'
import Search from '../Screens/Search/Search'
import Notification from '../Screens/Notification/Notification'
import Profile from '../Screens/Profile/Profile'

const Home_nav = createBottomTabNavigator(
    {
        Home:{
            screen: Home,
        },
        Search: {
            screen: Search,
        },
        Notification: {
            screen: Notification,
        },
        Profile: {
            screen: Profile
        },
    },
    {
        tabBarOptions: {
            showLabel: false,
        },
    }
);

export default Home_nav;