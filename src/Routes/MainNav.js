import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import {createStackNavigator} from 'react-navigation-stack';

import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import Home_nav from './Home_nav'


const AppStack = createStackNavigator({
    Home:{
        screen: Home_nav,
        navigationOptions: () => ({
            headerShown: false,
            headerBackTitle: null,
        }),
    }
},{
    initialRouteName: 'Home'
});

export default MainNav = createAppContainer(createSwitchNavigator({
    AppStack: AppStack,
},{
    initialRouteName:'AppStack'
}));