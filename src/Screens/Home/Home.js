import React, { Component } from 'react'

import {View, Text, StyleSheet, Dimensions, Image, SafeAreaView, RefreshControl, ScrollView, TextInput, TouchableWithoutFeedback} from 'react-native'

import Icon from 'react-native-vector-icons/dist/Feather'

import { Divider, Header } from 'react-native-elements';
import Tab_nav from '../../Routes/Home_inn_nav';
import { TouchableOpacity } from 'react-native-gesture-handler';

const {height, width} = Dimensions.get('window');

const TabIcon = (props) => (
    <Icon
      name={'home'}
      size={35}
      color= 'black'
    />
)

const Select = () => {
    return <View style={{height:5, backgroundColor:'lightblue'}}></View>;
}

export default class Home extends Component {
    static navigationOptions = {
        tabBarIcon: TabIcon
    };
    constructor(){
        super()
        this.state={
            activeTab: 'Recomended'
        }
    }
    render(){
        return(
            // <View style={styles.container}>
            //     <View style={styles.tabSelect}>
            //         <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            //             <TouchableOpacity onPress = {()=>{this.setState({activeTab:'Recomended'})}}>
            //             <View style={styles.tab_icn}>
            //                 <Text style={{...styles.tab_icn_txt, marginBottom: 10}}>Recomended</Text>
            //                 { this.state.activeTab === 'Recomended' ?
            //                     <Select /> : null 
            //                 }
            //             </View>
            //             </TouchableOpacity>
            //             <TouchableOpacity onPress = {()=>{this.setState({activeTab:'Movies'})}}>
            //             <View style={styles.tab_icn}>
            //                 <Text style={{...styles.tab_icn_txt, marginBottom: 10}}>Movies</Text>
            //                 { this.state.activeTab === 'Movies' ?
            //                     <Select /> : null 
            //                 }
            //             </View>
            //             </TouchableOpacity>
            //             <TouchableOpacity onPress = {()=>{this.setState({activeTab:'TVShows'})}}>
            //             <View style={styles.tab_icn}>
            //                 <Text style={{...styles.tab_icn_txt, marginBottom: 10}}>TVShows</Text>
            //                 { this.state.activeTab === 'TVShows' ?
            //                     <Select /> : null 
            //                 }
            //             </View>
            //             </TouchableOpacity>
            //             <TouchableOpacity onPress = {()=>{this.setState({activeTab:'Music'})}}>
            //             <View style={styles.tab_icn}>
            //                 <Text style={{...styles.tab_icn_txt, marginBottom: 10}}>Music</Text>
            //                 { this.state.activeTab === 'Music' ?
            //                     <Select /> : null 
            //                 }
            //             </View>
            //             </TouchableOpacity>
            //             <TouchableOpacity onPress = {()=>{this.setState({activeTab:'Podcast'})}}>
            //             <View style={styles.tab_icn}>
            //                 <Text style={{...styles.tab_icn_txt, marginBottom: 10}}>Podcast</Text>
            //                 { this.state.activeTab === 'Podcast' ?
            //                     <Select /> : null 
            //                 }
            //             </View>
            //             </TouchableOpacity>
            //         </ScrollView>
            //     </View>
            //     <View style={styles.content}>
            //         <ScrollView>
            //             {}
            //         </ScrollView>
            //     </View>
            // </View>
            <SafeAreaView style={{flex:1}}>
                <Tab_nav />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    tabSelect:{
        height:50
    },
    tab_icn:{
        height:50,
        justifyContent:'flex-end',
        backgroundColor:'#0f171e',
    },
    tab_icn_txt:{
        marginHorizontal:10,
        fontSize:18,
        color:'white'
    },
    content:{
        flex:1,
        backgroundColor:'#0f171e'
    },
});