import React, { Component } from 'react'

import {View, Text, StyleSheet, Dimensions, Image, SafeAreaView, RefreshControl, ScrollView, TextInput, TouchableWithoutFeedback} from 'react-native'

import Icon from 'react-native-vector-icons/dist/Feather'

import { Divider, Header } from 'react-native-elements';

const {height, width} = Dimensions.get('window');

const TabIcon = (props) => (
    <Text>Home</Text>
)

export default class Home_inner extends Component {
    static navigationOptions = {
        tabBarIcon: TabIcon
    };
    render(){
        return(
            <View style={styles.container}>
                <Text>Home_inner</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: '#0f171e'
    }
})