import React, { Component } from 'react'

import {View, Text, StyleSheet, Dimensions, Image, SafeAreaView, RefreshControl, ScrollView, TextInput, TouchableWithoutFeedback} from 'react-native'

import Icon from 'react-native-vector-icons/dist/Feather'

import { Divider, Header } from 'react-native-elements';

const {height, width} = Dimensions.get('window');

const TabIcon = (props) => (
    <Text>Podcast</Text>
)

export default class Podcast extends Component {
    static navigationOptions = {
        tabBarIcon: TabIcon
    };
    render(){
        return(
            <View>
                <Text>Podcast</Text>
            </View>
        );
    }
}